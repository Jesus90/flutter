import 'package:infoperlis/src/pages/home_page.dart';
import 'package:flutter/material.dart';
import 'package:infoperlis/src/pages/pelicula_detalle.dart';

void main() {
  runApp(const MainApp());
}

class MainApp extends StatelessWidget {
  const MainApp({super.key});

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'Peliculas',
      initialRoute: '/',
      routes: {
        '/': (BuildContext context) => homepage(),
        'detalle': (BuildContext context) => PeliculaDetalle(),
      },
    );
  }
}
