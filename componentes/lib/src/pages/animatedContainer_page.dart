import 'package:flutter/material.dart';
import 'dart:math';

// ignore: camel_case_types, use_key_in_widget_constructors
class animatedContainerpage extends StatefulWidget {
  @override
  animatedContainerState createState() => animatedContainerState();
}

// ignore: camel_case_types
class animatedContainerState extends State<animatedContainerpage> {
  double _width = 50.0;
  double _height = 50.0;
  Color _color = Colors.black;

  BorderRadiusGeometry _borderRadius = BorderRadius.circular(9.0);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('animated Container'),
      ),
      body: Center(
        child: AnimatedContainer(
          duration: Duration(seconds: 1),
          curve: Curves.fastEaseInToSlowEaseOut,
          width: _width,
          height: _height,
          decoration: BoxDecoration(borderRadius: _borderRadius, color: _color),
        ),
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: _cambiardeForma,
        child: Icon(Icons.play_arrow),
      ),
    );
  }

  void _cambiardeForma() {
    setState(() {
      final random = Random();

      _width = random.nextInt(350).toDouble();
      _height = random.nextInt(350).toDouble();

      _color = Color.fromRGBO(
          random.nextInt(255), random.nextInt(255), random.nextInt(255), 1);
      _borderRadius = BorderRadius.circular(random.nextInt(100).toDouble());
    });
  }
}
