import 'package:flutter/material.dart';

class AvatarPage extends StatelessWidget {
  const AvatarPage({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('avatar Page'),
        actions: <Widget>[
          _Avatar(),
        ],
      ),
      body: ListView(
        children: [_body()],
      ),
    );
  }

  Widget _Avatar() {
    return Container(
      margin: EdgeInsets.only(right: 10.0),
      child: CircleAvatar(
        //   backgroundImage: NetworkImage( 'https://scontent.fbog10-1.fna.fbcdn.net/v/t39.30808-6/250946853_3089721054644105_5925100905080904454_n.jpg?_nc_cat=104&ccb=1-7&_nc_sid=174925&_nc_eui2=AeFhPUFIOMZad9aEOYDjIjXYNiTLebYgnvg2JMt5tiCe-MKxyD1vJoiUyzUutPUuXUp5oCd8RmN2W4lE_4TSZR66&_nc_ohc=z9CfvY7QpvUAX99M3tx&_nc_pt=1&_nc_ht=scontent.fbog10-1.fna&oh=00_AfAXV2YIlps2V0WN-A82zQWVKpSIwy0uis9W1aJ2TqDXqw&oe=648E45F4'),
        child: Text('JD'),
        backgroundColor: Colors.brown,
      ),
    );
  }

  Widget _body() {
    return Center(
      child: FadeInImage(
        image: NetworkImage(
            'https://images.pexels.com/photos/1707215/pexels-photo-1707215.jpeg?auto=compress&cs=tinysrgb&w=1260&h=750&dpr=1'),
        placeholder: AssetImage('assets/loading/loading.gif'),
        fadeInDuration: Duration(milliseconds: 200),
      ),
    );
  }
}
