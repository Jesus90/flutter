import 'package:flutter/material.dart';
import 'package:infoperlis/src/models/paliculas_modelo.dart';

class cardHorizontal extends StatelessWidget {
  final List<Pelicula> peliculas;
  final Function siguientepagina;

  cardHorizontal({required this.peliculas, required this.siguientepagina});
  final _pageController =
      new PageController(initialPage: 1, viewportFraction: 0.2);

  @override
  Widget build(BuildContext context) {
    final _screenSize = MediaQuery.of(context).size;

    _pageController.addListener(
      () {
        if (_pageController.position.pixels >=
            _pageController.position.maxScrollExtent - 200) {
          siguientepagina();
        }
      },
    );

    return Container(
      height: _screenSize.height * 0.2,
      child: PageView.builder(
        pageSnapping: false,
        controller: _pageController,
        // children: _tarjetas(context),
        itemCount: peliculas.length,
        itemBuilder: (BuildContext context, index) {
          return _tarjeta(context, peliculas[index]);
        },
      ),
    );
  }

  Widget _tarjeta(BuildContext context, Pelicula pelicula) {
    final _screenSize2 = MediaQuery.of(context).size;

    pelicula.uniqueId = '${pelicula.id.toString()}-poster';

    final tarjeta = Container(
      margin: EdgeInsets.only(right: 15.0),
      child: Column(
        children: [
          Hero(
            tag: pelicula.uniqueId,
            child: ClipRRect(
              borderRadius: BorderRadius.circular(15.0),
              child: FadeInImage(
                placeholder: AssetImage('assets/img/image_icon.png'),
                fit: BoxFit.fill,
                image: NetworkImage(pelicula.getPosterImg()),
                height: _screenSize2.height * 0.17,
                width: _screenSize2.width * 0.16,
              ),
            ),
          ),
          Text(
            pelicula.title,
            overflow: TextOverflow.ellipsis,
            style: Theme.of(context).textTheme.bodySmall,
          )
        ],
      ),
    );

    return GestureDetector(
      child: tarjeta,
      onTap: () {
        //enrutando con argumentos
        Navigator.pushNamed(context, 'detalle', arguments: pelicula);
      },
    );
  }

//comentando esta segunda opcion

/*
  List<Widget> _tarjetas(BuildContext context) {
    final _screenSize2 = MediaQuery.of(context).size;

    return peliculas.map((Pelicula) {
      return Container(
        margin: EdgeInsets.only(right: 15.0),
        child: Column(
          children: [
            ClipRRect(
              borderRadius: BorderRadius.circular(15.0),
              child: FadeInImage(
                placeholder: AssetImage('assets/img/image_icon.png'),
                fit: BoxFit.fill,
                image: NetworkImage(Pelicula.getPosterImg()),
                height: _screenSize2.height * 0.17,
                width: _screenSize2.width * 0.16,
              ),
            ),
            Text(
              Pelicula.title,
              overflow: TextOverflow.ellipsis,
              style: Theme.of(context).textTheme.bodySmall,
            )
          ],
        ),
      );
    }).toList();
  }*/
}
