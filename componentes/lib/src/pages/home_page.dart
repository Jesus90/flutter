import 'package:componentes/src/utilis/icono_string_util.dart';
import 'package:flutter/material.dart';
import '../providers/menu_provider.dart';

class HomePages extends StatelessWidget {
  const HomePages({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Componentes '),
      ),
      body: _lista(),
    );
  }

  Widget _lista() {
    //usando listview builder

    return FutureBuilder(
      future: menuprovider.cargarData(),
      //  initialData: [],
      builder: (BuildContext context, AsyncSnapshot<List<dynamic>> snapshot) {
        //  print('context');
        // print(context);

        return ListView(
          children: _listaItems(snapshot, context),
        );
      },
    );
  }

  List<Widget> _listaItems(
      AsyncSnapshot<List<dynamic>> snapshot, BuildContext context) {
    final List<Widget> opciones = [];

    snapshot.data?.forEach((opt) {
      final widget = ListTile(
        title: Text(opt['texto']),
        leading: getIcons(opt['icon']),
        trailing: const Icon(Icons.keyboard_arrow_right),
        onTap: () {
          Navigator.pushNamed(context, opt['ruta']); //para multiples rutas
        },
      );

      opciones
        ..add(widget)
        ..add(const Divider());
    });

    return opciones;
  }
}
