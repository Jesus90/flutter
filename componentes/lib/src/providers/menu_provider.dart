import 'package:flutter/services.dart' show rootBundle;
import 'dart:convert'; //para deserealizar json

class _MenuProvider {
  List<dynamic> opciones = [];

  // ignore: empty_constructor_bodies
  _MenuProvider() {}

  Future<List<dynamic>> cargarData() async {
    final resp = await rootBundle.loadString('assets/data/menu_opts.json');
    Map dataMap = json.decode(resp);

    opciones = dataMap['rutas'];
    return opciones;
  }
}

final menuprovider = _MenuProvider();
