import 'package:componentes/src/pages/alert_page.dart';
import 'package:flutter/material.dart';

import 'package:componentes/src/rutas/routes.dart';
import 'package:flutter_localizations/flutter_localizations.dart';

void main() {
  runApp(const MainApp());
}

class MainApp extends StatelessWidget {
  const MainApp({super.key});

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Componente App',
      localizationsDelegates: [
        GlobalMaterialLocalizations.delegate,
        GlobalWidgetsLocalizations.delegate,
        GlobalCupertinoLocalizations.delegate,
      ],
      supportedLocales: [
        Locale('en'), // English
        Locale('es'), // Spanish
      ],
      debugShowCheckedModeBanner: false,
      // home: HomePages(),
      initialRoute: '/',
      routes: getApplicationRouter(),
      //en caso de que no exista una ruta
      onGenerateRoute: (RouteSettings setting) {
        return MaterialPageRoute(
            // ignore: prefer_const_constructors
            builder: (BuildContext context) => AlertPage());
      },
    );
  }
}
