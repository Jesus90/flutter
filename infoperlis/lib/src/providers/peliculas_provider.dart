import 'dart:async';
import 'dart:convert';

import 'package:infoperlis/src/models/actores_modelo.dart';
import 'package:infoperlis/src/models/paliculas_modelo.dart';
import 'package:http/http.dart' as http;

class PeliculasProvider {
  String _apikey = '7eafc7ecdf796a3d614688e72c7002cf';
  String _url = 'api.themoviedb.org';
  String _lang = 'es-ES';
  int _poularesPage = 0;

  bool _cargarpopulares = false;

//manejo de estado con stream
  List<Pelicula> _populares = <Pelicula>[];

  final _popularesStreamController =
      StreamController<List<Pelicula>>.broadcast();

  Function(List<Pelicula>) get popularesSink =>
      _popularesStreamController.sink.add;

  Stream<List<Pelicula>> get popularesStream =>
      _popularesStreamController.stream;

  ///--------------------------------------------

  void disposesStreams() {
    _popularesStreamController.close();
  }

  Future<List<Pelicula>> _procesarRespuesta(Uri url) async {
    final result = await http.get(url);
    final decodeData = json.decode(result.body);
    //print(decodeData['results']);
    final peliculas = new Peliculas.fromJsonList(decodeData['results']);
    // print(peliculas.items[3].title);
    return peliculas.items;
  }

  Future<List<Pelicula>> getEnCines() async {
    final url = Uri.https(
        _url, '3/movie/now_playing', {'language': _lang, 'api_key': _apikey});

    return _procesarRespuesta(url);
  }

  Future<List<Pelicula>> getPopulares() async {
    if (_cargarpopulares) return [];
    _cargarpopulares = true;

    _poularesPage++;

    final url = Uri.https(_url, '3/movie/popular', {
      'language': _lang,
      'api_key': _apikey,
      'page': _poularesPage.toString()
    });

    final resp = await _procesarRespuesta(url);

    _populares.addAll(resp);
    popularesSink(_populares);
    _cargarpopulares = false;
    return resp;
  }

  Future<List<Actor>> getCast(String pelidId) async {
    final url = Uri.https(_url, '3/movie/$pelidId/credits', {
      'language': _lang,
      'api_key': _apikey,
    });

    final resp = await http.get(url);
    final decodeData = json.decode(resp.body);

    final cast = new Cast.fromJsonList(decodeData['cast']);

    return cast.actores;
  }

  Future<List<Pelicula>> buscarPelicula(String query) async {
    final url = Uri.https(_url, '/3/search/movie',
        {'language': _lang, 'api_key': _apikey, 'query': query});

    return await _procesarRespuesta(url);
  }
}
