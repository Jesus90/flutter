import 'package:flutter/material.dart';
import 'package:qrscan/src/model/scan_model.dart';
import 'package:qrscan/src/pages/direcciones_page.dart';
import 'package:qrscan/src/pages/mapas_page.dart';

//
import 'package:qr_code_scanner/qr_code_scanner.dart';
import 'package:qrscan/src/providers/db_provider.dart';
import 'package:vibration/vibration.dart';

class HomePage extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  int currentIndex = 0;

  bool showScanQR = false;

  @override
  void initState() {
    super.initState();
  }

//qr
  final GlobalKey qrKey = GlobalKey(debugLabel: 'QR');
  late QRViewController controller;
  late bool scanningStarted = false;

  @override
  void dispose() {
    controller.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('QR Scanner'),
        backgroundColor: Theme.of(context).primaryColor,
        actions: [
          IconButton(onPressed: () {}, icon: Icon(Icons.delete_forever))
        ],
      ),
      body: showScanQR ? _scanQR() : _callPage(currentIndex),
      bottomNavigationBar: _crearBottomNavigationBar(context),
      floatingActionButtonLocation: FloatingActionButtonLocation.centerDocked,
      floatingActionButton: FloatingActionButton(
        child: Icon(Icons.filter_center_focus),
        onPressed: () {
          setState(() {
            showScanQR = !showScanQR;
          });
        },
        backgroundColor: Theme.of(context).primaryColor,
      ),
    );
  }

  void _onQRViewCreated(QRViewController controller) {
    setState(() {
      this.controller = controller;
    });
    controller.scannedDataStream.listen((scanData) async {
      if (!scanningStarted) {
        scanningStarted = true;
        // Aquí puedes manejar el resultado del escaneo

        try {
          Vibration.vibrate();
        } catch (e) {}

        //   print(scanData.code);

        String futureString = 'https://pub.dev/packages?q=vibrate&page=6';

        if (futureString != null) {
          final scan = ScanModel(valor: futureString);
          DBProvider.db.nuevoScanRaw(scan);
        }

        controller.pauseCamera();
        setState(() {
          showScanQR = false;
          scanningStarted = false;
        });
        // Puedes agregar lógica adicional después de escanear el código
      }
    });
  }

  Widget _scanQR() {
// ignore: unused_local_variable
    return Stack(
      children: [
        Expanded(
          flex: 5,
          child: QRView(
            key: qrKey,
            onQRViewCreated: _onQRViewCreated,
          ),
        ),
        Align(
          alignment: Alignment.center,
          child: Container(
            width: 200,
            height: 200,
            decoration: BoxDecoration(
              border: Border.all(
                color: Colors.red,
                width: 2.0,
              ),
            ),
          ),
        )
      ],
    );
  }

  //_callPage
  Widget _callPage(int paginaACtual) {
    switch (paginaACtual) {
      case 0:
        return MapasPege();

      case 1:
        return DirreccionesPege();

      default:
        return DirreccionesPege();
    }
  }

//_crearBottomNavigationBar

  Widget _crearBottomNavigationBar(BuildContext context) {
    return BottomNavigationBar(
      currentIndex: currentIndex,
      onTap: (index) {
        setState(() {
          currentIndex = index;
        });
      },
      items: [
        BottomNavigationBarItem(
            icon: Icon(Icons.map),
            label: 'Mapas',
            backgroundColor: Theme.of(context).primaryColor),
        BottomNavigationBarItem(
            icon: Icon(Icons.brightness_5),
            label: 'Dirrecciones',
            backgroundColor: Theme.of(context).primaryColor)
      ],
    );
  }
}
