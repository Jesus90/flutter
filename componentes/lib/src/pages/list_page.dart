import 'package:flutter/material.dart';

class Listapage extends StatefulWidget {
  const Listapage({super.key});

  @override
  _ListaPageState createState() => _ListaPageState();
}

class _ListaPageState extends State<Listapage> {
  List<int> _listaNumero = [1, 2, 3, 4, 5, 6, 7, 8, 9];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Lista'),
      ),
      body: _crearLista(),
    );
  }

  Widget _crearLista() {
    return ListView.builder(
      itemCount: _listaNumero.length,
      itemBuilder: (BuildContext context, int index) {
        final Image = _listaNumero[index];

        return FadeInImage(
            placeholder: AssetImage('assets/loading/loading.gif'),
            image: NetworkImage('https://picsum.photos/id/${Image}/500/300'));
      },
    );
  }
}
