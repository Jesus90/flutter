import 'dart:io';

import 'package:path_provider/path_provider.dart';
import 'package:path/path.dart';

import 'package:sqflite/sqflite.dart';

import 'package:qrscan/src/model/scan_model.dart';
export 'package:qrscan/src/model/scan_model.dart';

class DBProvider {
  static Database? _database;
  static final DBProvider db = DBProvider._private();

  DBProvider._private();

  get dataabase async {
    //que ya existe la base y la retorna
    if (_database != null) return _database;

    _database = await initDB();

    return _database;
  }

  initDB() async {
    Directory documentDirectory = await getApplicationDocumentsDirectory();
    final path = join(documentDirectory.path, 'ScansDB.db');
    return await openDatabase(path, version: 1, onOpen: (db) {},
        onCreate: (Database db, int version) async {
      await db.execute('CREATE TABLE SCANS ('
          'ID INTEGER PRIMARY KEY,'
          'TIPO TEXT,'
          'VALOR TEXT'
          ')');
    });
  }

  //craando registros
  nuevoScanRaw(ScanModel nuevoScan) async {
    final db = await dataabase;

    final res = await db.rawInsert('INSERT INTO SCANS (ID,TIPO,VALOR) '
        'VALUES (${nuevoScan.id}, ${nuevoScan.tipo},${nuevoScan.valor})');

    return res;
  }

  Future<int> nuevoScan(ScanModel nuevoScan) async {
    final db = await dataabase;
    final res = await db.insert('SCANS', nuevoScan.toJson());
    return res;
  }

//select
  Future<ScanModel?> getScanId(int id) async {
    final db = await dataabase;
    final res = await db.query('SCANS', where: 'id=?', whereArgs: [id]);
    return res.isNotEmty ? ScanModel.fromJson(res.first) : null;
  }

  List<ScanModel> scanModelListFromJson(List<dynamic> jsonList) {
    return jsonList.map((json) => ScanModel.fromJson(json)).toList();
  }

//select all
  Future<List<ScanModel>> getAllScans() async {
    final db = await dataabase;
    final res = await db.query('SCANS');
    List<ScanModel> list = scanModelListFromJson(res);

    return list;
  }

//select all  for tipo
  Future<List<ScanModel>> getAllScansTipo(String tipo) async {
    final db = await dataabase;
    final res = await db
        .rawQuery("SELECT * FROM SCANS WHERE TIPO='$tipo'"); //query('SCANS');
    List<ScanModel> list = res.isNotEmpty
        ? res.map((item) => ScanModel.fromJson(item)).toList()
        : [];

    return list;
  }

//actulizar registros
  Future<int> updateScan(ScanModel nuevoScan) async {
    final db = await dataabase;
    final res = await db.update('SCANS', nuevoScan.toJson(),
        where: 'id=?', whereArgs: [nuevoScan.id]);

    return res;
  }

//eliminar Registors

  Future<int> deleteScan(int id) async {
    final db = await dataabase;
    final res = await db.delete('SCANS', where: 'id=?', whereArgs: [id]);
    return res;
  }

  Future<int> deleteAll() async {
    final db = await dataabase;
    final res = await db.rawDelete('DELETE FROM SCANS ');
    return res;
  }
}
