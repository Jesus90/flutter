import 'package:flutter/material.dart';
import 'package:infoperlis/src/models/paliculas_modelo.dart';
import 'package:infoperlis/src/providers/peliculas_provider.dart';

class DataSearch extends SearchDelegate {
  final peliculasprovider = new PeliculasProvider();

  String seleccion = "";

  final pelicula = [
    'iron man 1',
    'iron man 2',
    'iron man 3',
    'iron man 4',
  ];

  final peliculasReciente = ['Superman', 'Capitan America'];

  //acciones
  @override
  List<Widget>? buildActions(BuildContext context) {
    return [
      IconButton(
          onPressed: () {
            query = '';
          },
          icon: Icon(Icons.clear))
    ];
  }

  //miientras carga
  @override
  Widget? buildLeading(BuildContext context) {
    return IconButton(
        onPressed: () {
          close(context, null);
        },
        icon: AnimatedIcon(
            icon: AnimatedIcons.menu_arrow, progress: transitionAnimation));
  }

//resultado
  @override
  Widget buildResults(BuildContext context) {
    return Center(
      child: Container(
        height: 100.0,
        width: 100.0,
        color: Colors.blueAccent,
        child: Text(seleccion),
      ),
    );
  }

//sugerencias
  @override
  Widget buildSuggestions(BuildContext context) {
    if (query.isEmpty) {
      return Container();
    }

    return FutureBuilder(
        future: peliculasprovider.buscarPelicula(query),
        builder:
            (BuildContext context, AsyncSnapshot<List<Pelicula>> snapshot) {
          if (snapshot.hasData) {
            final peliculas = snapshot.data;

            return ListView(
                children: peliculas!.map((pelicula) {
              return ListTile(
                leading: FadeInImage(
                  image: NetworkImage(pelicula.getPosterImg()),
                  placeholder: AssetImage('assets/img/image_icon.png'),
                  width: 50.0,
                  fit: BoxFit.contain,
                ),
                title: Text(pelicula.title),
                subtitle:
                    Text(pelicula.originalTitle + '  ' + pelicula.releaseDate),
                onTap: () {
                  close(context, null);
                  pelicula.uniqueId = "";
                  Navigator.pushNamed(context, 'detalle', arguments: pelicula);
                },
              );
            }).toList());
          } else {
            return Center(child: CircularProgressIndicator());
          }
        });

//
    //  final listaSugerida = (query.isEmpty)
    //      ? peliculasReciente
    //      : pelicula
    //          .where((p) => p.toLowerCase().startsWith(query.toLowerCase()))
    //          .toList();
//
    //  return ListView.builder(
    //      itemCount: listaSugerida.length,
    //      itemBuilder: (context, index) {
    //        return ListTile(
    //          leading: Icon(Icons.movie),
    //          title: Text(listaSugerida[index]),
    //          onTap: () {
    //            seleccion = listaSugerida[index];
    //            showResults(context);
    //          },
    //        );
    //      });
    //
  }
}
