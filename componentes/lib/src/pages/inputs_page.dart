import 'package:flutter/material.dart';

class InputPage extends StatefulWidget {
  @override
  _InputPageState createState() => _InputPageState();
}

class _InputPageState extends State<InputPage> {
  String _nombre = '';
  String _email = '';
  String _password = '';
  String _fecha = '';
  String _opcionselect = '';

  TextEditingController _inputFieldDateController = new TextEditingController();

  List<String> _poderes = ['Correr', 'volar', 'salto', 'rayo laser'];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Inputs de texto'),
      ),
      body: ListView(
        padding: EdgeInsets.symmetric(horizontal: 10.0, vertical: 20.0),
        children: [
          Column(
            children: [
              _crearInput(),
              Divider(),
              _crearEmail(),
              Divider(),
              _CrearPasswords(),
              Divider(),
              Crearfecha(context),
              Divider(),
              _crearDropdown(),
              Divider(),
              _crearPpersona(),
            ],
          )
        ],
      ),
    );
  }

  Widget _crearInput() {
    return TextField(
      textCapitalization: TextCapitalization.sentences,
      decoration: InputDecoration(
          border: OutlineInputBorder(borderRadius: BorderRadius.circular(20.0)),
          counter: Text('Letras ${_nombre.length} '),
          hintText: 'Nombre de la persona',
          labelText: 'Nombre',
          helperText: 'Solo es el Nombre',
          suffixIcon: Icon(Icons.accessibility),
          icon: Icon(Icons.account_circle)),
      onChanged: (valor) {
        setState(() {
          _nombre = valor;
        });
      },
    );
  }

  Widget _crearEmail() {
    return TextField(
      // textCapitalization: TextCapitalization.sentences,
      keyboardType: TextInputType.emailAddress,
      decoration: InputDecoration(
          border: OutlineInputBorder(borderRadius: BorderRadius.circular(20.0)),
          counter: Text('Letras ${_password.length} '),
          hintText: 'Email de la persona',
          labelText: 'Email',
          helperText: 'Solo es el Email',
          suffixIcon: Icon(Icons.email),
          icon: Icon(Icons.email_rounded)),
      onChanged: (valor) {
        setState(() {
          _password = valor;
        });
      },
    );
  }

  Widget _CrearPasswords() {
    return TextField(
      // textCapitalization: TextCapitalization.sentences,
      // keyboardType: TextInputType.emailAddress,
      obscureText: true,
      decoration: InputDecoration(
          border: OutlineInputBorder(borderRadius: BorderRadius.circular(20.0)),
          counter: Text('Letras ${_email.length} '),
          hintText: 'Contrase;a de la persona',
          labelText: 'Contrase;a',
          helperText: 'Solo es el Contrase;a',
          suffixIcon: Icon(Icons.lock),
          icon: Icon(Icons.lock)),
      onChanged: (valor) {
        setState(() {
          _email = valor;
        });
      },
    );
  }

  Widget Crearfecha(BuildContext context) {
    return TextField(
      // textCapitalization: TextCapitalization.sentences,
      // keyboardType: TextInputType.emailAddress,
      // obscureText: true,
      controller: _inputFieldDateController,
      enableInteractiveSelection: false,
      decoration: InputDecoration(
          border: OutlineInputBorder(borderRadius: BorderRadius.circular(20.0)),
          counter: Text('Letras ${_email.length} '),
          hintText: 'fecha de la persona',
          labelText: 'fecha',
          helperText: 'Solo es el Contrase;a',
          suffixIcon: Icon(Icons.date_range),
          icon: Icon(Icons.calendar_today)),
      onTap: () {
        setState(() {
          FocusScope.of(context).requestFocus(new FocusNode());
          _selectDate(context);
        });
      },
    );
  }

  _selectDate(BuildContext context) async {
    DateTime? picked = await showDatePicker(
        context: context,
        initialDate: new DateTime.now(),
        firstDate: new DateTime(2018),
        lastDate: new DateTime(2029),
        locale: Locale('es', 'ES'));

    if (picked != null) {
      setState(() {
        _fecha = picked.toString();
        _inputFieldDateController.text = _fecha;
      });
    } /* else {
      setState(() {
        _inputFieldDateController.text = '';
      });
    }*/
  }

  Widget _crearDropdown() {
    return Row(children: [
      Icon(Icons.select_all),
      Expanded(
          child: DropdownButton<String>(
              value: _opcionselect,
              items: getOpcionesDropdown(),
              onChanged: (opt) {
                setState(() {
                  _opcionselect = opt!;
                });
              })),
    ]);
  }

  List<DropdownMenuItem<String>> getOpcionesDropdown() {
    List<DropdownMenuItem<String>> lista = [];

    lista.add(DropdownMenuItem(
      value: '', // Valor vacío
      child: Text('Seleccione'), // Texto para la opción vacía
    ));

    for (var element in _poderes) {
      lista.add(DropdownMenuItem(
        value: element,
        child: Text(element),
      ));
    }

    return lista;
  }

  Widget _crearPpersona() {
    return ListTile(
      title: Text('Nombre De la Persona  $_nombre'),
      subtitle: Text('El Email es: $_email '),
    );
  }
}
