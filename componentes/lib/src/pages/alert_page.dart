import 'package:flutter/material.dart';

class AlertPage extends StatelessWidget {
  const AlertPage({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Alerta Page'),
      ),
      body: Center(
        child: ElevatedButton(
          onPressed: () => _mostrarAlerta(context),
          // ignore: prefer_const_constructors
          child: Text('Mostrar Alerta'),
        ),
      ),
    );
  }

  void _mostrarAlerta(BuildContext context) {
    showDialog(
        context: context,
        barrierDismissible: true,
        builder: (context) {
          return AlertDialog(
            shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(20.0)),
            // ignore: prefer_const_constructors
            title: Text('Titulo'),
            // ignore: prefer_const_constructors
            content: Column(
              mainAxisSize: MainAxisSize.min,
              // ignore: prefer_const_constructors, prefer_const_literals_to_create_immutables
              children: [Text('contenido de alerta '), FlutterLogo(size: 100)],
            ),
            actions: [
              TextButton(
                  onPressed: () => Navigator.of(context).pop(),
                  // ignore: prefer_const_constructors
                  child: Text('Cancelar')),
              TextButton(
                  onPressed: () {
                    Navigator.of(context).pop();
                  },
                  // ignore: prefer_const_constructors
                  child: Text('ok'))
            ],
          );
        });
  }
}
