import 'package:flutter/material.dart';

class SliderPage extends StatefulWidget {
  const SliderPage({super.key});

  @override
  _SliderPageState createState() => _SliderPageState();
}

class _SliderPageState extends State<SliderPage> {
  double _valorSlider = 100.0;
  bool _bloquearCheck = false;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Slider'),
      ),
      body: Container(
        padding: EdgeInsetsDirectional.only(top: 30.0),
        child: Column(
          children: [
            _createslider(),
            Divider(),
            _checkBox(),
            Divider(),
            _crearSwitch(),
            Expanded(child: _crearImagen()),
          ],
        ),
      ),
    );
  }

  Widget _createslider() {
    return Slider(
        activeColor: Colors.indigoAccent,
        label: 'the size picture is : $_valorSlider',
        value: _valorSlider,
        min: 1.0,
        max: 100.0,
        onChanged: _bloquearCheck
            ? null
            : (valor) {
                setState(() {
                  _valorSlider = valor;
                });
              });
  }

  Widget _crearImagen() {
    return Image(
        width: _valorSlider * 4,
        fit: BoxFit.contain,
        image: NetworkImage(
            'https://i.pinimg.com/originals/60/a9/61/60a96199afa8469b7c3c46810ed86816.png'));
  }

  Widget _checkBox() {
//opcion 1
/*    return Checkbox(
        value: _bloquearCheck,
        onChanged: (opt) {
          setState(() {
            _bloquearCheck = opt!;
          });
        });

*/
    return CheckboxListTile(
        title: Text('Bloquear Slider  con checkbox'),
        value: _bloquearCheck,
        onChanged: (opt) {
          setState(() {
            _bloquearCheck = opt!;
          });
        });
  }

  Widget _crearSwitch() {
    return SwitchListTile(
        title: Text('Bloquear Slider con switch'),
        value: _bloquearCheck,
        onChanged: (opt) {
          setState(() {
            _bloquearCheck = opt;
          });
        });
  }
}
