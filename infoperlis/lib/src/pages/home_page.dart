import 'package:flutter/material.dart';

import 'package:infoperlis/src/providers/peliculas_provider.dart';
import 'package:infoperlis/src/search/search_delegate.dart';
import 'package:infoperlis/src/widgets/card_horizontal.dart';
import 'package:infoperlis/src/widgets/card_swiper_widget.dart';

class homepage extends StatelessWidget {
  homepage({super.key});
  final peliculasprovider = new PeliculasProvider();
  @override
  Widget build(BuildContext context) {
    peliculasprovider.getPopulares();
    return Scaffold(
        appBar: AppBar(
          centerTitle: false,
          title: Text('Peliculas en cines'),
          backgroundColor: Colors.indigoAccent,
          actions: [
            IconButton(
                onPressed: () {
                  showSearch(
                      context: context, delegate: DataSearch()); //--> buscador
                },
                icon: Icon(Icons.search))
          ],
        ),
        body: Container(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.spaceAround,
            children: [_swiperTarjetas(), _footer(context)],
          ),
        ));
  }

  Widget _swiperTarjetas() {
    return FutureBuilder(
      future: peliculasprovider.getEnCines(),
      //  initialData: initialData,
      builder: (BuildContext context, AsyncSnapshot snapshot) {
        if (snapshot.data != null) {
          return cardSwiper(peliculas: snapshot.data);
        } else {
          return Container(
              height: 400.0, child: Center(child: CircularProgressIndicator()));
        }
      },
    );
  }

  Widget _footer(BuildContext context) {
    return Container(
        width: double.infinity,
        child: Column(crossAxisAlignment: CrossAxisAlignment.start, children: [
          Container(
              padding: EdgeInsets.only(left: 20.0),
              child: Text('Populares',
                  style: Theme.of(context).textTheme.bodyMedium)),
          SizedBox(height: 3.0),
          StreamBuilder(
            stream: peliculasprovider.popularesStream,
            builder: (BuildContext context, AsyncSnapshot snapshot) {
              if (snapshot.data != null) {
                return cardHorizontal(
                  peliculas: snapshot.data,
                  siguientepagina: peliculasprovider.getPopulares,
                );
              } else {
                return Container(
                    height: 400.0,
                    child: Center(child: CircularProgressIndicator()));
              }
            },
          )

          /*FutureBuilder(
            future: peliculasprovider.getPopulares(),
            builder: (BuildContext context, AsyncSnapshot snapshot) {
              if (snapshot.data != null) {
                return cardHorizontal(peliculas: snapshot.data);
              } else {
                return Container(
                    height: 400.0,
                    child: Center(child: CircularProgressIndicator()));
              }
            },
          )*/
        ]));
  }
}
