import 'package:flutter/material.dart';

//flutter_swiper_view
import 'package:flutter_swiper_view/flutter_swiper_view.dart';
import 'package:infoperlis/src/models/paliculas_modelo.dart';

class cardSwiper extends StatelessWidget {
  final List<Pelicula> peliculas;

  cardSwiper({required this.peliculas});

  @override
  Widget build(BuildContext context) {
//media query
    final _screenSize = MediaQuery.of(context).size;

    return Container(
      padding: EdgeInsets.only(top: 8.0),
      width: _screenSize.width,
      //height: _screenSize.height * 0.5,
      child: Swiper(
        itemBuilder: (BuildContext context, int index) {
          peliculas[index].uniqueId =
              '${peliculas[index].id.toString()}-tarjeta';

          return Hero(
            tag: peliculas[index].uniqueId,
            child: ClipRRect(
                borderRadius: BorderRadius.circular(20.0),
                child: GestureDetector(
                  onTap: () => Navigator.pushNamed(context, 'detalle',
                      arguments: peliculas[index]),
                  child: FadeInImage(
                      placeholder: AssetImage('assets/img/image_icon.png'),
                      fit: BoxFit.fill,
                      image: NetworkImage(peliculas[index].getPosterImg())),
                )),
          );
        },
        itemCount: peliculas.length,
        //pagination: const SwiperPagination(),
        // control: const SwiperControl(),
        layout: SwiperLayout.TINDER,
        itemWidth: _screenSize.width * 0.7,
        itemHeight: _screenSize.height * 0.5,
      ),
    );
  }
}
