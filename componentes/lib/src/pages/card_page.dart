import 'package:flutter/material.dart';

class CartPage extends StatelessWidget {
  const CartPage({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Cart Page'),
      ),
      body: ListView(
        padding: EdgeInsets.all(20),
        children: <Widget>[
          _cartTipo1(),
          SizedBox(height: 30.3),
          _cartTipo2(),
          _cartTipo1(),
          SizedBox(height: 30.3),
          _cartTipo2(),
        ],
      ),
    );
  }

//_cartTipo1
  Widget _cartTipo1() {
    // ignore: prefer_const_constructors
    return Card(
      elevation: 20.0,
      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(10)),
      // ignore: prefer_const_constructors
      child: Column(
        // ignore: prefer_const_literals_to_create_immutables
        children: [
          // ignore: prefer_const_constructors
          ListTile(
            // ignore: prefer_const_constructors
            leading: Icon(
              Icons.photo_album,
              color: Colors.blue,
            ),
            title: const Text(
              'Titulo de la tarjeta',
            ),
            subtitle: const Text('Subtitulo de la tarjeta'),
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.end,
            children: [
              TextButton(onPressed: () => {}, child: const Text('Cancelar')),
              TextButton(onPressed: () => {}, child: const Text('Aceptar'))
            ],
          )
        ],
      ),
    );
  }

  Widget _cartTipo2() {
    final container = Container(
      child: Column(
        children: <Widget>[
          FadeInImage(
            placeholder: AssetImage('assets/loading/loading.gif'),
            image: NetworkImage(
                'https://images3.alphacoders.com/131/1311991.jpeg'),
            fadeInDuration: Duration(milliseconds: 200),
            fit: BoxFit.cover,
          ),

          // ignore: prefer_const_constructors
          //  Image(        // ignore: prefer_const_constructors
          //  image: NetworkImage(
          //   'https://images3.alphacoders.com/131/1311991.jpeg')),
          Container(
              // ignore: prefer_const_constructors
              padding: EdgeInsets.all(10.0),
              // ignore: prefer_const_constructors
              child: Text('texto de la imagen que se esta cargado  '))
        ],
      ),
    );

    return Card(
      // ignore: sort_child_properties_last
      child: ClipRRect(
        // ignore: sort_child_properties_last
        child: container,
        borderRadius: BorderRadius.circular(10.0),
      ),
      elevation: 20.0,
      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(10)),
    );

    //otra forma
    /*  container(
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(30.0),
          color: Colors.while
          boxShadow: <BoxShadow>[
            BoxShadow(color: Colors.red  , blurRadius: 10.0 ,spreadRadius: 2.0 ,offset: Offset(2.0, 10.0)  )
          ] 
          //color: Colors.red
        ),
        child: ClipRRect(
          borderRadius: BorderRadius.circular(30.0),
          child: container,
        ));*/
  }
}
