import 'package:componentes/src/pages/list2_page.dart';
import 'package:componentes/src/pages/list_page.dart';
import 'package:flutter/material.dart';
import 'package:componentes/src/pages/alert_page.dart';
import 'package:componentes/src/pages/animatedContainer_page.dart';
import 'package:componentes/src/pages/avatar_page.dart';
import 'package:componentes/src/pages/card_page.dart';
import 'package:componentes/src/pages/home_page.dart';
import 'package:componentes/src/pages/inputs_page.dart';
import 'package:componentes/src/pages/slider_page.dart';

Map<String, WidgetBuilder> getApplicationRouter() {
  final rutas = <String, WidgetBuilder>{
    // ignore: prefer_const_constructors
    '/': (BuildContext context) => HomePages(),
    // ignore: prefer_const_constructors
    'alert': (BuildContext context) => AlertPage(),
    // ignore: prefer_const_constructors
    'avatar': (BuildContext context) => AvatarPage(),
    // ignore: prefer_const_constructors
    'card': (BuildContext context) => CartPage(),
    // ignore: prefer_const_constructors
    'slider': (BuildContext context) => SliderPage(),
    // ignore: prefer_const_constructors
    'animatedContainer': (BuildContext context) => animatedContainerpage(),
    'inputs': (BuildContext context) => InputPage(),
    'list': (BuildContext context) => Listapage(),
    'list2': (BuildContext context) => Listapageautoscroll()
  };

  return rutas;
}
