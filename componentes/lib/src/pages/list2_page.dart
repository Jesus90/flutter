import 'package:flutter/material.dart';
import 'dart:async';

class Listapageautoscroll extends StatefulWidget {
  const Listapageautoscroll({super.key});

  @override
  _ListaPageState createState() => _ListaPageState();
}

class _ListaPageState extends State<Listapageautoscroll> {
  List<int> _listaNumero = [];
  int _ultimonumero = 0;

  bool _isloading = false;

  ScrollController _scrollcontroller = new ScrollController();

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    _agregar10();
    _scrollcontroller.addListener(() {
      if (_scrollcontroller.position.pixels ==
          _scrollcontroller.position.maxScrollExtent) {
        // _agregar10();
        _fetchData();
      }
    });
  }

//prevenir fugas de memoria;
  @override
  void dispose() {
    super.dispose();
    _scrollcontroller.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Lista'),
      ),
      body: Stack(
        children: [_crearLista(), _crearLoading()],
      ),
    );
  }

  Widget _crearLista() {
    return RefreshIndicator(
      onRefresh: obtenerMasImagenes,
      child: ListView.builder(
        controller: _scrollcontroller,
        itemCount: _listaNumero.length,
        itemBuilder: (BuildContext context, int index) {
          final Image = _listaNumero[index];

          return FadeInImage(
            placeholder: AssetImage('assets/loading/loading.gif'),
            image: NetworkImage('https://picsum.photos/id/${Image}/500/300'),
          );
        },
      ),
    );
  }

  Future obtenerMasImagenes() async {
    final duracion = new Duration(seconds: 2);
    new Timer(duracion, () {
      _listaNumero.clear();
      _ultimonumero++;
      _agregar10();
    });

    return Future.delayed(duracion);
  }

  void _agregar10() {
    for (int i = 0; i < 10; i++) {
      _ultimonumero++;
      _listaNumero.add(_ultimonumero);
      setState(() {});
    }
  }

  Future _fetchData() async {
    _isloading = true;
    setState(() {});
    final duration = new Duration(seconds: 2);
    //cuendo pasa 2 segundo llama el metodo RespuestaHttp
    return new Timer(duration, RespuestaHttp);
  }

  void RespuestaHttp() {
    _isloading = false;

    _scrollcontroller.animateTo(_scrollcontroller.position.pixels + 100,
        duration: Duration(milliseconds: 250), curve: Curves.fastOutSlowIn);

    _agregar10();
  }

  Widget _crearLoading() {
    if (_isloading) {
      return Column(
        mainAxisSize: MainAxisSize.max,
        mainAxisAlignment: MainAxisAlignment.end,
        children: [
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [CircularProgressIndicator()],
          ),
          SizedBox(
            height: 15.0,
          )
        ],
      );
    } else {
      return Container();
    }
  }
}
